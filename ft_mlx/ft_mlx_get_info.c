/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_mlx_get_info.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: piquerue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/17 04:44:37 by piquerue          #+#    #+#             */
/*   Updated: 2017/05/17 05:12:15 by piquerue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../libft.h"

void	ft_mlx_get_size(char *argv)
{
	char	**split;
	int		i;
	int		w;
	int		h;

	i = 0;
	w = 1280;
	h = 720;
	split = ft_strsplit(argv, 'x');
	while (split[i])
	{
		if (i == 0)
			w = ft_atoi(split[i]);
		else if (i == 1)
			h = ft_atoi(split[i]);
		else
		{
			ft_printf("Erreur: trop de parametres pour la taille de la fenetres\n");
			ft_printf("--size [WIDTH]x[HEIGHT]\n");
		}
		i++;
	}
}

void	ft_mlx_extended_parser(int argc, char **argv)
{
	int i;

	i = 2;
	if (argc <= 2)
		return ;
	if (argc % 2 != 0)
		return ;
	while (i < argc)
	{
		if (ft_strcmp(argv[i], "--size") == 0)
			ft_mlx_get_size(argv[i + 1]);
		i += 2;
	}
}
