/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_mlx_menu_color.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: piquerue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/17 05:12:28 by piquerue          #+#    #+#             */
/*   Updated: 2017/05/17 09:57:21 by piquerue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../libft.h"

long	ft_power(long n, size_t power)
{
	if (power == 0)
		return (1);
	if (power == 1)
		return (n);
	return (n * ft_power(n, power - 1));
}

t_point	create_pt(int x, int y)
{
	t_point		pt;

	pt.x = x;
	pt.y = y;
	return (pt);
}

t_color_mlx		create_color(int red, int green, int blue)
{
	t_color_mlx col;

	col.red = red;
	col.green = green;
	col.blue = blue;
	return (col);
}

void	ft_sphere(t_fractol *fractol, t_point pt, int r)
{
	t_point tmp;

	tmp = pt;
	tmp.y -= r;
	while (tmp.y <= pt.y + r)
	{
		tmp.x = pt.x - r;
		while (tmp.x <= pt.x + r)
		{
			if (r * r >= ft_power(tmp.x - pt.x, 2) + ft_power(tmp.y - pt.y, 2))
				ft_pixel_put3(fractol, tmp, create_color(0x00, 0xFF, 0x00));
			tmp.x++;
		}
		tmp.y++;
	}
}

void	ft_line(t_point pt, t_fractol *fractol, int h, int i)
{
	t_point tmp;

	tmp = pt;
	tmp.y = h;
	while (pt.x < pt.y)
	{
		ft_pixel_put3(fractol, create_pt(pt.x, h), create_color(0x00, 0xFF, 0x00));
		pt.x++;
	}
	if (i == 2)
		tmp.x = (fractol->color_bg.red * (pt.y - tmp.x) / 255) + tmp.x;
	if (i == 3)
		tmp.x = (fractol->color_bg.green * (pt.y - tmp.x) / 255) + tmp.x;
	if (i == 4)
		tmp.x = (fractol->color_bg.blue * (pt.y - tmp.x) / 255) + tmp.x;
	if (i == 10)
		tmp.x = (fractol->color_pt.red * (pt.y - tmp.x) / 255) + tmp.x;
	if (i == 11)
		tmp.x = (fractol->color_pt.green * (pt.y - tmp.x) / 255) + tmp.x;
	if (i == 12)
		tmp.x = (fractol->color_pt.blue * (pt.y - tmp.x) / 255) + tmp.x;
	ft_sphere(fractol, tmp, 5);
}


void	ft_zone(t_fractol *fractol, t_point a, t_point b, t_color_mlx col)
{
	t_point tmp;

	tmp = create_pt(a.x, a.y);
	while (tmp.x <= b.x)
	{
		tmp = create_pt(tmp.x, a.y);
		while (tmp.y <= b.y)
		{
			if (tmp.x == a.x || tmp.y == a.y || tmp.x == b.x || tmp.y == b.y)
				ft_pixel_put3(fractol, tmp, create_color(0xFF, 0xFF, 0xFF));
			else
				ft_pixel_put3(fractol, tmp, col);
			tmp.y++;
		}
		tmp.x++;
	}
}

void	display_menu(t_fractol *fractol)
{
	t_point pt;
	int h;
	int l;

	l = (fractol->win->width / 4);
	h = 1;
	pt = create_pt(l * 3, 0);
	while (pt.x < fractol->win->width)
	{
		pt.y = 0;
		while (pt.y < fractol->win->height)
		{
			ft_pixel_put(fractol, pt);
			pt.y++;
		}
		pt.x++;
	}
	while (h++ <= 3)
		ft_line(create_pt((l * 0.1) + l * 3, (l * 0.9) + l * 3), fractol, ((h * pt.y) / 19), h);
	ft_zone(fractol, create_pt((l / 5) + l * 3, 5 * fractol->win->height / 19), create_pt(((l - l / 5) + l * 3), 9 * fractol->win->height / 19), fractol->color_bg);
	h = 9;
	while (h++ <= 11)
		ft_line(create_pt((l * 0.1) + l * 3, (l * 0.9) + l * 3), fractol, ((h * pt.y) / 19), h);
	ft_zone(fractol, create_pt((l / 5) + l * 3, 13 * fractol->win->height / 19), create_pt(((l - l / 5) + l * 3), 17 * fractol->win->height / 19), fractol->color_pt);
}
