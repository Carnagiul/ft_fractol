/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fractol_threads.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: piquerue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/17 02:19:02 by piquerue          #+#    #+#             */
/*   Updated: 2017/05/17 10:41:56 by piquerue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../libft.h"

int		ft_print_fractal_complex(t_core *core, t_complex x, t_point p)
{
	if (core->fractol->fractal == 0)
		julia(core, x, p);
	else if (core->fractol->fractal == 1)
		mandelbrot(core, x, p);
	else if (core->fractol->fractal == 2)
		burning_ship(core, x, p);
	else if (core->fractol->fractal == 3)
		fractal_bonus_1(core, x, p);
	else if (core->fractol->fractal == 4)
		fractal_bonus_2(core, x, p);
	else if (core->fractol->fractal == 5)
		fractal_bonus_3(core, x, p);
	else
		julia(core, x, p);
	return (1);
}

void	print_fractal(t_core *core)
{
	t_complex	x;
	t_point		p;

	if (core->fractol->fractal <= 5)
	{
		p.y = 0;
		while (p.y < core->fractol->win->height)
		{
			p.x = core->min;
			x.i = (double)p.y / core->fractol->zoom + core->fractol->zoom_i +
				core->fractol->move_x;
			while (p.x < core->max)
			{
				x.r = (double)p.x / core->fractol->zoom -
					core->fractol->zoom_r + core->fractol->move_y;
				p.x += ft_print_fractal_complex(core, x, p);
			}
			p.y++;
		}
	}
}

void	ft_fractol_print_clear(t_fractol *fractol)
{
	if (fractol->alive == 1)
	{
		mlx_destroy_image(fractol->win->mlx, fractol->img->img_ptr);
		free(fractol->img);
	}
	fractol->alive = 1;
	fractol->img = ft_mlx_extended_gen_img(fractol->win);
}

void	ft_fractol_print(t_fractol *fractol)
{
	pthread_t	thread[4];
	t_core		core[4];
	int			i;
	int			max;

	i = -1;
	max = fractol->win->width / 4;
	ft_fractol_print_clear(fractol);
	while (++i < 4)
	{
		core[i].fractol = fractol;
		core[i].min = max * i;
		core[i].max = max * (i + 1);
		pthread_create(&thread[i], NULL, (void*)print_fractal, &core[i]);
	}
	i = 0;
	while (i < 4)
		pthread_join(thread[i++], NULL);
	if (fractol->menu == 1)
		display_menu(fractol);
	mlx_put_image_to_window(fractol->win->mlx, fractol->win->win,
			fractol->img->img_ptr, 0, 0);
}
