/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fractol_calc.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: piquerue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/17 01:00:31 by piquerue          #+#    #+#             */
/*   Updated: 2017/05/17 01:09:24 by piquerue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../libft.h"

void	julia(t_core *core, t_complex z, t_point p)
{
	t_complex	c;
	t_complex	tmp;
	int			i;

	c.r = core->fractol->z.r;
	c.i = core->fractol->z.i;
	i = 0;
	while (z.r * z.r + z.i * z.i < 4 && i < core->fractol->iteration_max)
	{
		tmp = z;
		z.r = tmp.r * tmp.r - tmp.i * tmp.i + c.r;
		z.i = 2 * tmp.r * tmp.i + c.i;
		i++;
	}
	ft_pixel_put2(core, p, i);
}

void	mandelbrot(t_core *core, t_complex c, t_point p)
{
	t_complex	z;
	t_complex	tmp;
	int			i;

	z.r = core->fractol->z.r;
	z.i = core->fractol->z.i;
	i = 0;
	while (z.r * z.r + z.i * z.i < 4 && i < core->fractol->iteration_max)
	{
		tmp = z;
		z.r = tmp.r * tmp.r - tmp.i * tmp.i + c.r;
		z.i = 2 * tmp.r * tmp.i + c.i;
		i++;
	}
	ft_pixel_put2(core, p, i);
}

void	burning_ship(t_core *core, t_complex c, t_point p)
{
	t_complex	z;
	t_complex	tmp;
	int			i;

	z.r = core->fractol->z.r;
	z.i = core->fractol->z.i;
	i = 0;
	while (z.r * z.r + z.i * z.i < 4 && i < core->fractol->iteration_max)
	{
		tmp.r = (z.r >= 0) ? z.r : -z.r;
		tmp.i = (z.i >= 0) ? z.i : -z.i;
		z.r = tmp.r * tmp.r - tmp.i * tmp.i + c.r;
		z.i = 2 * tmp.r * tmp.i + c.i;
		i++;
	}
	ft_pixel_put2(core, p, i);
}
