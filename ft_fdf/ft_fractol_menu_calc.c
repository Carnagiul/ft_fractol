/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fractol_menu_calc.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: piquerue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/17 09:15:08 by piquerue          #+#    #+#             */
/*   Updated: 2017/05/17 10:48:05 by piquerue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../libft.h"

void	ft_fractol_menu_calc(t_fractol *fractol, int x, int y)
{
	int data[4];
	int	h;
	t_point pt;

	h = 2;
	data[0] = fractol->win->width / 4;
	data[1] = (data[0] * 0.1) + (data[0] * 3);
	data[2] = (data[0] * 0.9) + (data[0] * 3);
	while (h <= 4)
	{
		if (h == 2)
			pt.x = (fractol->color_bg.red * (data[2] - data[1]) / 255) + data[1];
		if (h == 3)
			pt.x = (fractol->color_bg.green * (data[2] - data[1]) / 255) + data[1];
		if (h == 4)
			pt.x = (fractol->color_bg.blue * (data[2] - data[1]) / 255) + data[1];
		data[3] = (h * fractol->win->height) / 19;
		pt.y = data[3];
		if (((y >= pt.y - 5) && (y <= pt.y + 5)) && (x >= pt.x - 5 && pt.x + 5 >= x))
		{
			fractol->id = h;
			//ft_printf("TU TOUCHE LE PREMIER CURSEUR\n");
			return ;
		}
		h++;
	}
	h = 10;
	while (h <= 12)
	{
		if (h == 10)
			pt.x = (fractol->color_pt.red * (data[2] - data[1]) / 255) + data[1];
		if (h == 11)
			pt.x = (fractol->color_pt.green * (data[2] - data[1]) / 255) + data[1];
		if (h == 12)
			pt.x = (fractol->color_pt.blue * (data[2] - data[1]) / 255) + data[1];
		data[3] = (h * fractol->win->height) / 19;
		pt.y = data[3];
		if (((y >= pt.y - 5) && (y <= pt.y + 5)) && (x >= pt.x - 5 && pt.x + 5 >= x))
		{
			fractol->id = h;
			//ft_printf("TU TOUCHE LE SECOND CURSEUR\n");
			return ;
		}
		h++;
	}
}

int		ft_fractol_release(int k, int x, int y, t_fractol *fractol)
{
	int data[4];
	t_point pt;

	//ft_printf("cocou %d %d %d \n", fractol->menu, fractol->id, k);
	if (fractol->menu == 1 && fractol->id != -1 && k == 1)
	{
		data[0] = fractol->win->width / 4;
		data[1] = (data[0] * 0.1) + (data[0] * 3);
		data[2] = (data[0] * 0.9) + (data[0] * 3);
		data[3] = (fractol->id * fractol->win->height) / 19;
		pt.y = data[3];
		if (((y >= pt.y - 5) && (y <= pt.y + 5)) && (x >= data[1] && data[2] >= x))
		{
			data[3] = (data[2] - data[1]) - (data[2] - x);
			if (fractol->id == 2)
				fractol->color_bg.red = data[3];
			if (fractol->id == 3)
				fractol->color_bg.green = data[3];
			if (fractol->id == 4)
				fractol->color_bg.blue = data[3];
			if (fractol->id == 10)
				fractol->color_pt.red = data[3];
			if (fractol->id == 11)
				fractol->color_pt.green = data[3];
			if (fractol->id == 12)
				fractol->color_pt.blue = data[3];
			fractol->id = -1;
		}
	}
	ft_fractol_print(fractol);
	return (0);
}
