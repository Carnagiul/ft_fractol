/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fractol_hooks_3.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: piquerue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/16 23:14:11 by piquerue          #+#    #+#             */
/*   Updated: 2017/05/17 10:03:31 by piquerue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../libft.h"

void	ft_fractol_select_bonus_1(t_fractol *fractol)
{
	fractol->fractal = 4;
}

void	ft_fractol_select_bonus_2(t_fractol *fractol)
{
	fractol->fractal = 5;
}

void	ft_fractol_select_bonus_3(t_fractol *fractol)
{
	fractol->fractal = 6;
}

void	ft_fractol_select_bonus_4(t_fractol *fractol)
{
	fractol->fractal = 7;
}

int		ft_fractol_zoom_mouse(int keycode, int x, int y, t_fractol *fractol)
{
	long double xi;
	long double yi;

	xi = ((double)fractol->win->width / fractol->zoom) /
		(fractol->win->width * 3);
	yi = ((double)fractol->win->height / fractol->zoom) /
		(fractol->win->height * 3);
	if (keycode == 4 || keycode == 5)
	{
		if (keycode == 5)
		{
			fractol->move_y += (xi * (double)x);
			fractol->move_x += (yi * (double)y);
			fractol->zoom *= 1.5;
		}
		else
		{
			fractol->move_y -= (xi * 1.5 * (double)x);
			fractol->move_x -= (yi * 1.5 * (double)y);
			fractol->zoom *= 1 / 1.5;
		}
	}
	if (fractol->menu == 1 && keycode == 1)
		ft_fractol_menu_calc(fractol, x, y);
	ft_fractol_print(fractol);
	return (0);
}
