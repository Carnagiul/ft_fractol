/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fractol_hooks_manager.c                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: piquerue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/16 23:22:36 by piquerue          #+#    #+#             */
/*   Updated: 2017/05/17 10:45:46 by piquerue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../libft.h"
#include "../ft_fdf_map.h"

void		ft_fractol_reset(t_fractol *fractol)
{
	fractol = ft_fractol_init_him(fractol);
}

int			ft_mouse(int x, int y, t_fractol *fractol)
{
	if (x >= 1280 || x < 0 || y >= 720 || y < 0 || fractol->space)
		return (0);
	fractol->z.r = (double)x / (double)1280;
	fractol->z.i = (double)y / (double)720;
	ft_fractol_print(fractol);
	return (1);
}

int			ft_mlx_hooker(int keycode, t_fractol *fractol)
{
	int		i;

	i = 0;
	while (i < 21)
	{
		if (keycode == g_t_fdf_map[i].key)
		{
			ft_printf("la touche @Greconnu@@ a etait presse\nTouche: @P%S@@\n",
				g_t_fdf_map[i].key_display);
			g_t_fdf_map[i].f(fractol);
			break ;
		}
		i++;
	}
	if (keycode == 76)
	{
		fractol->menu = (fractol->menu == 1) ? 0 : 1;
		if (fractol->menu)
			fractol->id = -1;
	}
	if (i == 21)
	{
		ft_printf("Une touche @Rnon reconnu@@ a etait presser\n");
		ft_printf("Key_code : @G%d@@\n", keycode);
	}
	ft_fractol_print(fractol);
	return (0);
}

void		ft_mlx_start_hooks(t_fractol *fractol)
{
	mlx_key_hook(fractol->win->win, ft_mlx_hooker, fractol);
	mlx_hook(fractol->win->win, 6, (1L << 6), ft_mouse, fractol);
	mlx_hook(fractol->win->win, 4, (1L << 2), ft_fractol_zoom_mouse, fractol);
	mlx_hook(fractol->win->win, 5, (1L << 3), ft_fractol_release, fractol);
	mlx_loop(fractol->win->mlx);
}
