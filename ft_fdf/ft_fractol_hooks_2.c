/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fractol_hooks_2.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: piquerue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/16 23:09:10 by piquerue          #+#    #+#             */
/*   Updated: 2017/05/16 23:44:09 by piquerue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../libft.h"

void	ft_fractol_esc(t_fractol *fractol)
{
	ft_printf("Fermeture du projet fractol\n");
	exit(0);
	(void)fractol;
}

void	ft_fractol_select_julia(t_fractol *fractol)
{
	fractol->fractal = 0;
}

void	ft_fractol_select_mandelbrot(t_fractol *fractol)
{
	fractol->fractal = 1;
}

void	ft_fractol_select_burning_ship(t_fractol *fractol)
{
	fractol->fractal = 2;
}

void	ft_fractol_select_bonus_0(t_fractol *fractol)
{
	fractol->fractal = 3;
}
