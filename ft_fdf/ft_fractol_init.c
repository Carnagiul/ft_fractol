/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fractol_init.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: piquerue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/15 02:04:25 by piquerue          #+#    #+#             */
/*   Updated: 2017/05/17 09:59:34 by piquerue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../libft.h"

int				ft_fractol_error(char *str)
{
	ft_printf("%s\n", str);
	exit(1);
}

int				ft_fractol_parser(char *arg)
{
	int			strlen;

	strlen = ft_strlen(arg);
	if (strlen != 1)
	{
		ft_fractol_error("Usage: ./fractol [J, M, B, 0, 1, 2]");
		return (-1);
	}
	else
	{
		if (arg[0] == 'J' || arg[0] == 'M')
			return ((arg[0] == 'J') ? 0 : 1);
		else if (arg[0] == 'B' || arg[0] == '0')
			return ((arg[0] == 'B') ? 2 : 3);
		else if (arg[0] == '1' || arg[0] == '2')
			return ((arg[0] == '1') ? 4 : 5);
		ft_fractol_error("Usage: ./fractol [J, M, B, 0, 1, 2]");
		return (0);
	}
	ft_fractol_error("Usage: ./fractol [J, M, B, 0, 1, 2]");
	return (-1);
}

t_fractol		*ft_fractol_init_him(t_fractol *fractol)
{
	fractol->iteration_max = 50;
	fractol->zoom = 300;
	fractol->move_x = 0;
	fractol->move_y = 0;
	fractol->zoom_r = 1.733333;
	fractol->zoom_i = -1.233333;
	fractol->space = 0;
	fractol->z = ft_t_complex_init(0, 0.65);
	fractol->alive = 0;
	fractol->fractal = 0;
	fractol->color_bg = create_color(0x00, 0x00, 0x00);
	fractol->color_pt = create_color(0xFF, 0xFF, 0xFF);
	fractol->menu = 0;
	fractol->id = -1;
	return (fractol);
}

void			ft_fractol_init2(char **argv)
{
	t_win		*win;
	t_fractol	*fractol;

	win = ft_mlx_extended_gen_win(1280, 720, "mlx");
	fractol = (t_fractol *)malloc(sizeof(t_fractol));
	if (!fractol)
	{
		free(win->win);
		free(win->mlx);
		free(win);
		ft_fractol_error("Erreur lors du malloc de fractol");
		exit(1);
	}
	else
		fractol = ft_fractol_init_him(fractol);
	fractol->fractal = ft_fractol_parser(argv[1]);
	fractol->win = win;
	ft_mlx_start_hooks(fractol);
}

int				ft_fractol_init(int argc, char **argv)
{
	ft_mlx_extended_parser(argc, argv);
	if (argc != 2)
	{
		ft_fractol_error("Usage: ./fractol [J, M, B, 0, 1, 2]");
		return (1);
	}
	else
	{
		if (ft_fractol_parser(argv[1]) >= 0)
			ft_fractol_init2(argv);
		exit(0);
	}
	return (0);
}
