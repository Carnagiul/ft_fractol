/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fractol_hooks_1.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: piquerue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/16 23:05:16 by piquerue          #+#    #+#             */
/*   Updated: 2017/05/16 23:33:01 by piquerue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../libft.h"

void	ft_fractol_increment_iteration(t_fractol *fractol)
{
	fractol->iteration_max += 10;
}

void	ft_fractol_decrement_iteration(t_fractol *fractol)
{
	fractol->iteration_max -= 10;
}

void	ft_fractol_zoom_in_key(t_fractol *fractol)
{
	fractol->zoom *= 1.1;
}

void	ft_fractol_zoom_out_key(t_fractol *fractol)
{
	fractol->zoom *= 0.9;
}

void	ft_fractol_space(t_fractol *fractol)
{
	fractol->space = (fractol->space) ? 0 : 1;
}
