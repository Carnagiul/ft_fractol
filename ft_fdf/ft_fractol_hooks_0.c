/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fractol_hooks_0.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: piquerue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/16 22:55:23 by piquerue          #+#    #+#             */
/*   Updated: 2017/05/17 06:01:24 by piquerue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../libft.h"

void	ft_fractol_key_left(t_fractol *fractol)
{
	fractol->zoom_r += -50 / fractol->zoom;
}

void	ft_fractol_key_right(t_fractol *fractol)
{
	fractol->zoom_r += 50 / fractol->zoom;
}

void	ft_fractol_key_up(t_fractol *fractol)
{
	fractol->zoom_i += -50 / fractol->zoom;
}

void	ft_fractol_key_down(t_fractol *fractol)
{
	fractol->zoom_i += 50 / fractol->zoom;
}
