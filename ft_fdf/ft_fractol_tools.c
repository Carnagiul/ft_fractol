/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fractol_tools.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: piquerue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/17 01:04:49 by piquerue          #+#    #+#             */
/*   Updated: 2017/05/17 10:53:39 by piquerue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../libft.h"

void	ft_pixel_put2(t_core *core, t_point p, int i)
{
	int chute;
	int red;
	int green;
	int blue;

	red = i * core->fractol->color_pt.red / (255 - core->fractol->color_bg.red);
	green = i * core->fractol->color_pt.green / (255 -core->fractol->color_bg.green);
	blue = i * core->fractol->color_pt.blue / (255 - core->fractol->color_bg.blue);
	if (i == 0)
	{
		red = core->fractol->color_bg.red;
		green = core->fractol->color_bg.green;
		blue = core->fractol->color_bg.blue;
	}
	chute = ((int)p.x * 4) + ((int)p.y * core->fractol->img->size_line);
	core->fractol->img->img[chute] = red;
	core->fractol->img->img[++chute] = green;
	core->fractol->img->img[++chute] = blue;
}

void	ft_pixel_put(t_fractol *fractol, t_point pt)
{
	int chute;

	chute = (pt.x * 4) + (pt.y * fractol->img->size_line);
	fractol->img->img[chute] = 255;
	fractol->img->img[++chute] = 120;
	fractol->img->img[++chute] = 0;
	fractol->img->img[++chute] = 90;
}

void	ft_pixel_put3(t_fractol *fractol, t_point pt, t_color_mlx color)
{
	int chute;

	chute = (pt.x * 4) + (pt.y * fractol->img->size_line);
	fractol->img->img[chute] = color.red;
	fractol->img->img[++chute] = color.green;
	fractol->img->img[++chute] = color.blue;
	fractol->img->img[++chute] = 90;
}
