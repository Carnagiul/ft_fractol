/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_math.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: piquerue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/18 19:55:40 by piquerue          #+#    #+#             */
/*   Updated: 2016/12/18 23:59:56 by piquerue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_MATH_H
# define FT_MATH_H

int			ft_pow(int nb);
int			ft_math_sum(int a, int b);
int			ft_math_factorial(int nb);
int			ft_math_mod(int a, int div);
int			ft_math_div(int a, int div);
int			ft_math_abs(int nb);
long long	ft_pow_l(long long nbr);

#endif
