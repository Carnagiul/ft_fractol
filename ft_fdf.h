/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fdf.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: piquerue <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/30 23:11:51 by piquerue          #+#    #+#             */
/*   Updated: 2017/05/17 10:16:50 by piquerue         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_FDF_H
# define FT_FDF_H

# include <pthread.h>

typedef struct	s_color_mlx
{
	int			red;
	int			green;
	int			blue;
}				t_color_mlx;

typedef struct			s_fractol
{
	struct s_win		*win;
	struct s_img		*img;
	int					iteration_max;
	double				zoom;
	double				move_x;
	double				move_y;
	double				zoom_i;
	double				zoom_r;
	int					alive;
	int					space;
	int					fractal;
	struct s_color_mlx	color_bg;
	struct s_color_mlx	color_pt;
	struct s_complex	z;
	int					menu;
	int					id;
}						t_fractol;

typedef struct			s_core
{
	struct s_fractol	*fractol;
	int					min;
	int					max;
}						t_core;

typedef struct			s_fdf_map
{
	int					key;
	wchar_t				*key_display;
	void				(*f)(t_fractol *fractol);
}						t_fdf_map;

int						ft_fractol_init(int argc, char **argv);

/*
** FT_FRACTOL_HOOKS_0.c
*/
void					ft_fractol_key_left(t_fractol *fractol);
void					ft_fractol_key_right(t_fractol *fractol);
void					ft_fractol_key_up(t_fractol *fractol);
void					ft_fractol_key_down(t_fractol *fractol);

/*
** FT_FRACTOL_HOOKS_1.c
*/

void					ft_fractol_increment_iteration(t_fractol *fractol);
void					ft_fractol_decrement_iteration(t_fractol *fractol);
void					ft_fractol_zoom_in_key(t_fractol *fractol);
void					ft_fractol_zoom_out_key(t_fractol *fractol);
void					ft_fractol_space(t_fractol *fractol);

/*
** FT_FRACTOL_HOOKS_2.c
*/

void					ft_fractol_esc(t_fractol *fractol);
void					ft_fractol_select_julia(t_fractol *fractol);
void					ft_fractol_select_mandelbrot(t_fractol *fractol);
void					ft_fractol_select_burning_ship(t_fractol *fractol);
void					ft_fractol_select_bonus_0(t_fractol *fractol);

/*
** FT_FRACTOL_HOOKS_3.c
*/

void					ft_fractol_select_bonus_1(t_fractol *fractol);
void					ft_fractol_select_bonus_2(t_fractol *fractol);
void					ft_fractol_select_bonus_3(t_fractol *fractol);
void					ft_fractol_select_bonus_4(t_fractol *fractol);
int						ft_fractol_zoom_mouse(int keycode, int x,
		int y, t_fractol *fractol);

/*
** FT_FRACTOL_HOOKS_MANAGER.c
*/

int						ft_mouse(int x, int y, t_fractol *fractol);
int						ft_mlx_hooker(int keycode, t_fractol *fractol);
void					ft_fractol_reset(t_fractol *fractol);
void					ft_mlx_start_hooks(t_fractol *fractol);
/*
** FT_FRACTAL_CALC.c
*/

void					julia(t_core *core, t_complex z, t_point p);
void					mandelbrot(t_core *core, t_complex c, t_point p);
void					burning_ship(t_core *core, t_complex c, t_point p);

/*
** FT_FRACTAL_CALC_BONUS.c
*/

void					fractal_bonus_1(t_core *core, t_complex z, t_point p);
void					fractal_bonus_2(t_core *core, t_complex c, t_point p);
void					fractal_bonus_3(t_core *core, t_complex c, t_point p);

/*
** FT_FRACTOL_TOOLS.c
*/

void					ft_pixel_put2(t_core *core, t_point p, int i);
void					ft_pixel_put(t_fractol *fractol, t_point p);
void					ft_pixel_put3(t_fractol *fractol, t_point p, t_color_mlx color);
int						ft_fractol_release(int keycode, int x, int y, t_fractol *fractol);

/*
** FT_FRACTOL_THEADS.c
*/

int						ft_print_fractal_complex(t_core *core, t_complex x,
		t_point p);
void					print_fractal(t_core *core);
void					ft_fractol_print(t_fractol *fractol);
t_fractol				*ft_fractol_init_him(t_fractol *fractol);

/*
** FT_FRACTOL_MENU_CALC.c
*/

void					ft_fractol_menu_calc(t_fractol *fractol, int x, int y);

#endif
